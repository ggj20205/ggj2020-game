extends Node2D

var text_preload = {
	"happy" : [
		"Hello Sir I need my item repaired. What a lovely shop you have here.",
		"I need this fixed and I hear you are the best smith around.",
		"Can you please repair this item? You're a sweetie!",
		"Sir I am in a hurry please make this item a priority.",
		"Ah Greg it has been too long! Can you please fix this up for me?",
		"Hey hotstuff I need this fixed and I need you. ;)",
		"My good man I require this repaired at the upmost haste!",
	],
	"normal" : [
		"Please Sir if this takes much longer I will be late!",
		"Can you hurry it up? I am a very important person you know.",
		"I understand these things take time, but this is getting ridiculous!",
		"When is my item going to be done smithy?",
		"I am not paying you to stand around, get my item fixed!",
		"My Lord will tan my hide if I am late! You must hurry Sir!",
	],
	"angry" : [
		"That is it! You are going to lose my business and I am a very influential person!",
		"Are you working with two broken arms!? Hurry it up!",
		"My child could do this faster!",
		"This place is a sham! You are not getting any money from me!",
		"It is a good thing you look cute because that would be the only reason I will come here again.",
		"#$%@ you!",
		"You are the worst smithy I have ever seen!",
	],
}

var texture_preload = [
	preload("res://assets/textbox/Box.png"),
	preload("res://assets/textbox/Customer_One.png"),
	preload("res://assets/textbox/Customer_Two.png"),
	preload("res://assets/textbox/Customer_Three.png"),
	preload("res://assets/textbox/Customer_Four.png"),
	preload("res://assets/textbox/Customer_Five.png"),
]

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func show(yesno:=true):
	self.visible = yesno
	if (yesno):
		$Timer.start()
	else:
		$Timer.stop()

func random_text(mood):
	self.text_preload[mood].shuffle()
	self.set_text(text_preload[mood][0])

func set_text(text):
	$TextBox.clear()
	$TextBox.add_text(text)

func set_portrait(portraitnum):
	$PortraitTex.set_texture(texture_preload[portraitnum])


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func is_showing():
	return self.visible

func _on_Timer_timeout():
	self.show(false)
