extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum directions { UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT }
var use_frame = 0
var current_dir;
export (int) var speed = 19200
var motion = Vector2()

func cartesian_to_isometric(cartesian):
	var screen_pos = Vector2()
	screen_pos.y = (cartesian.x + cartesian.y) / 2
	screen_pos.x = (cartesian.x - cartesian.y) * 1.7 / 2
	return screen_pos

func get_input():
	var direction = Vector2()
	if Input.is_action_pressed("up"):
		use_frame = 0
		if $"../".held_item == null:
			$AnimatedSprite.play("walk_up_right")
		else:
			$AnimatedSprite.play("walk_up_right_item")
		current_dir = directions.UP_RIGHT;
		direction += Vector2(0,-1)
		$Area2D/CollisionShape2D.rotation_degrees = 60
	elif Input.is_action_pressed("down"):
		use_frame = 0
		if $"../".held_item == null:
			$AnimatedSprite.play("walk_down_left")
		else:
			$AnimatedSprite.play("walk_down_left_item")
		current_dir = directions.DOWN_LEFT;
		direction += Vector2(0,1)
		$Area2D/CollisionShape2D.rotation_degrees = -120
	elif Input.is_action_pressed("left"):
		use_frame = 0
		if $"../".held_item == null:
			$AnimatedSprite.play("walk_up_left")
		else:
			$AnimatedSprite.play("walk_up_left_item")
		current_dir = directions.UP_LEFT;
		direction += Vector2(-1,0)
		$Area2D/CollisionShape2D.rotation_degrees = -60
	elif Input.is_action_pressed("right"):
		use_frame = 0
		if $"../".held_item == null:
			$AnimatedSprite.play("walk_down_right")
		else:
			$AnimatedSprite.play("walk_down_right_item")
		current_dir = directions.DOWN_RIGHT;
		direction += Vector2(1,0)
		$Area2D/CollisionShape2D.rotation_degrees = 120
	else:
		$AnimatedSprite.stop()
	motion = direction.normalized()

	if Input.is_action_just_pressed("ui_accept"):
		var temp = $Area2D.get_overlapping_bodies()
		if (len(temp) > 1):
			get_parent().use_station(temp[0])
			if get_parent().held_item == null:
				match current_dir:
					directions.DOWN_LEFT:
						$AnimatedSprite.set_animation("use_down_left")
						$AnimatedSprite.set_frame(use_frame)
						use_frame = (use_frame + 1) %2
					directions.DOWN_RIGHT:
						$AnimatedSprite.set_animation("use_down_right")
						$AnimatedSprite.set_frame(use_frame)
						use_frame = (use_frame + 1) %2
					directions.UP_RIGHT:
						$AnimatedSprite.set_animation("use_up_right")
						$AnimatedSprite.set_frame(use_frame)
						use_frame = (use_frame + 1) %2
					directions.UP_LEFT:
						$AnimatedSprite.set_animation("use_up_left")
						$AnimatedSprite.set_frame(use_frame)
						use_frame = (use_frame + 1) %2
			else:
				match current_dir:
					directions.DOWN_LEFT:
						$AnimatedSprite.play("walk_down_left_item")
					directions.DOWN_RIGHT:
						$AnimatedSprite.play("walk_down_right_item")
					directions.UP_RIGHT:
						$AnimatedSprite.play("walk_up_right_item")
					directions.UP_LEFT:
						$AnimatedSprite.play("walk_down_left_item")

func _physics_process(delta):
	get_input()
	check_station_collision()
	
	motion = motion * speed * delta
	motion = cartesian_to_isometric(motion)

	move_and_slide(motion)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_name():
	return "Player"

func check_station_collision():
	var temp = $Area2D.get_overlapping_bodies()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
