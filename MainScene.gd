extends Node2D

var screen_size

var MIN_SPAWN_TIME = 2
var MAX_SPAWN_TIME = 10

var texture_preload = {
	"anvil" : preload("res://assets/Anvil.png"),
	"forge" : preload("res://assets/Furnace_Off.png"),
	"forge-off" : preload("res://assets/Furnace_Off.png"),
	"forge-on" : preload("res://assets/Furnace_On.png"),
	"grind" : preload("res://assets/Grindstone.png"),
	"polish" : preload("res://assets/Goldsmith.png"),
	"quench" : preload("res://assets/Quench.png"),
	"workbench" : preload("res://assets/Workbench.png"),
	"counter" : preload("res://assets/Counter.png"),
	"table" : preload("res://assets/Table.png"),
	"box" : preload("res://assets/Box.png"),
	"saddle-broke" : preload("res://assets/BrknSddl.png"),
	"saddle-fixed" : preload("res://assets/FixdSddl.png"),
	"pot-broke" : preload("res://assets/BrknPot.png"),
	"pot-fixed" : preload("res://assets/FixdPot.png"),
	"sword-broke" : preload("res://assets/BrknSwrd.png"),
	"sword-fixed" : preload("res://assets/FixdSwrd.png"),
	"horseshoe-broke" : preload("res://assets/BrknHrsSh.png"),
	"horseshoe-fixed" : preload("res://assets/FixdHrsSh.png"),
	"mace-broke" : preload("res://assets/BrknMc.png"),
	"mace-fixed" : preload("res://assets/FixdMc.png"),
	"dagger-broke" : preload("res://assets/BrknDggr.png"),
	"dagger-fixed" : preload("res://assets/FixdDggr.png"),
	"platearmour-broke" : preload("res://assets/BrknArmr.png"),
	"platearmour-fixed" : preload("res://assets/FixdArmr.png"),
	"leatherarmour-broke" : preload("res://assets/BrknLthArmr.png"),
	"leatherarmour-fixed" : preload("res://assets/FixdLthArmr.png"),
}

var item_preload = {
	"saddle" : preload("res://Items/Saddle.gd"),
	"pot" : preload("res://Items/Pot.gd"),
	"sword" : preload("res://Items/Sword.gd"),
	"horseshoe" : preload("res://Items/Horseshoe.gd"),
	"mace" : preload("res://Items/Mace.gd"),
	"dagger" : preload("res://Items/Dagger.gd"),
	"platearmour" : preload("res://Items/PlateArmour.gd"),
	"leatherarmour" : preload("res://Items/LeatherArmour.gd"),
}
var rng = RandomNumberGenerator.new()
func random_item_class():
	rng.randomize()
	return self.item_preload.values()[rng.randi_range(0, self.item_preload.size()-1)]

var customer = preload("res://Customer.tscn")

var counters = []
var customers = []

var score = 0
var dialogue_box

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	
	self.dialogue_box = get_node('/root/DialogueBox')

	# Get counters
	counters = get_tree().get_nodes_in_group('counters')

	for i in range(len(counters)):
		customers.append(null)

	# Start customer spawn timer
	$CustomerSpawnTimer.start()

func _on_CustomerSpawnTimer_timeout():

	# Select next available counter
	var next_available = -1

	# Loop through customers and check if there is
	# a free slot for a new one to come in
	for i in range(len(customers)):
		if customers[i] == null:
			next_available = i

	# Check if there is a counter available
	if next_available < 0:
		return
		
	$CustomerSpawnTimer.wait_time = rng.randf_range(MIN_SPAWN_TIME, MAX_SPAWN_TIME)

	# Create a new customer
	var c = customer.instance()
	c.init(counters[next_available], random_item_class().new())

	# Set y position to that of the counter
	var counter = self.counters[next_available]
	c.position = counter.position - Vector2.LEFT.rotated(deg2rad(150)).normalized() * 200
	c.z_index = 3

	print('spawing player at:', c.position.x, ' - ', c.position.y)
	
	c.connect('customer_mood', self, '_on_customer_mood')
	c.connect('customer_purchase', self, '_on_customer_purchase')
	c.connect('customer_exit', self, '_on_customer_exit')

	customers[next_available] = c

	add_child(c)

func _on_customer_purchase(score):
	self.score += score
	$ScoreLabel.text = 'Score: ' + str(self.score)
	$PurchaseAudio.play()

func _on_customer_exit(c):
	print('customer exited:', c)

	for i in range(len(customers)):
		if customers[i] == c:
			customers[i] = null

	c.queue_free()

func _on_customer_mood(customer, mood):
	print('customer mood:', customer, ' - ', customer.portrait, ' - ', mood)
	
	if !dialogue_box.is_showing():
		dialogue_box.random_text(mood)
		dialogue_box.set_portrait(customer.portrait)
		dialogue_box.show(true)
		
	
