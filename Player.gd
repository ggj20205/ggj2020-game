extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var held_item = null

# Called when the node enters the scene tree for the first time.
func _ready():
	# self.held_item = $"..".random_item_class().new()
	self.update_item_sprite()

func use_station(station):
	if (station == null):
		return
	
	if station is Station:
		self.held_item = station.interact(self.held_item)
		self.update_item_sprite()

func update_item_sprite():
	if (self.held_item == null):
		$KinematicBody2D/ItemSprite.visible = false
		$"../NextStep".visible = false
	else:
		var temp = self.held_item.get_next_step()
		$KinematicBody2D/ItemSprite.set_texture(self.held_item.texture)
		$KinematicBody2D/ItemSprite.visible = true
		if (temp != null):
			$"../NextStep".set_texture($"..".texture_preload[temp])
			$"../NextStep".visible = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
