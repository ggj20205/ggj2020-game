extends Station

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

signal item_changed

# TODO: Add function to see if item already on counter

func peek_item():
	return self.held_item

func interact(i):
	# If not trying to get item,
	# put the texture on the sprite.
	if i != null:
		$ItemSprite.visible = true
	else:
		$ItemSprite.visible = false
	
	var tmp = .interact(i)
	
	emit_signal('item_changed')
	
	return tmp
