extends Node2D

var original_scale

# Called when the node enters the scene tree for the first time.
func _ready():
	self.original_scale = $Bar.scale

func set_percent(p):
	$Bar.scale.y = (original_scale.y * p)

# Sets the color of the bar
func set_color(c):
	$Bar.modulate = c
