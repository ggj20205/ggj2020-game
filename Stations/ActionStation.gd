class_name ActionStation
extends Station

func interact(item):
	var retval = null
	if (self.held_item != null):
		
		if self.held_item.status[station_name].needs > 0:
			self.action_performed()
		else:
			self.action_finished()
			
		if (self.held_item.status[station_name].do()):
			retval = self.held_item
			self.held_item = item
		else:
			return item
	else:
		self.held_item = item
	self.update_item_sprite()
	return retval
	
func action_performed():
	pass
	
func action_finished():
	pass
