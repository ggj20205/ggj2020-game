extends TimedStation

func _init():
	self.station_name = "quench"

func update_activity_state(state):
	$SteamSprite.visible = state
	
	if state == true and !$QuenchSound.is_playing():
		$QuenchSound.play()
