extends TimedStation

var texture_preload = {
	false : preload("res://assets/Furnace_Off.png"),
	true  : preload("res://assets/Furnace_On.png"),
}

func _init():
	self.station_name = "forge"

func update_activity_state(state):
	$Sprite.set_texture(self.texture_preload[state])
