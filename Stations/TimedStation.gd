class_name TimedStation
extends Station

func _process(delta):
	if (self.held_item):
		self.update_activity_state(!self.held_item.status[station_name].do(delta))

func update_activity_state(state):
	pass
