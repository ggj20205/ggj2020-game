extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "_on_player_enter")

func _on_player_enter(body):
	print("_on_player_enter: " + body.get_name())
	if(body.get_name() == "Player"):
		print("Player is in back of station!")
		print("original PZ: " + str(body.z_index))
		print("original SZ: " + str(get_parent().z_index))
		body.z_index = 1
		get_parent().z_index = 2 
		print("new PZ: " + str(body.z_index))
		print("new SZ: " + str(get_parent().z_index))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
