class_name Station
extends StaticBody2D

var held_item = null
var station_name = ""

func _ready():
	update_item_sprite()

func interact(item):
	var retval = self.held_item
	self.held_item = item
	self.update_item_sprite()
	return retval

func update_item_sprite():
	if (self.held_item == null):
		$ItemSprite.visible = false
	else:
		$ItemSprite.set_texture(self.held_item.texture)
		$ItemSprite.visible = true
