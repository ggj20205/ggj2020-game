extends ActionStation

func _init():
	self.station_name = "grind"

func action_performed():
	if !$GrindNoise.is_playing():
		$GrindNoise.play()

func action_finished():
	if $GrindNoise.is_playing():
		$GrindNoise.stop()
