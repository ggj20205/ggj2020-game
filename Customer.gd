extends KinematicBody2D

var STATE_NONE = 0
var STATE_APPROACH = 1
var STATE_WAITING = 2
var STATE_TAKING = 3
var STATE_EXIT = 4

var TEMPERAMENT_MILD = 0
var TEMPERAMENT_MEDIUM = 1
var TEMPERAMENT_AGGRESSIVE = 2

var MODELS = 3

var MIN_WAIT_TIME = 30
var MAX_WAIT_TIME = 60

signal customer_mood
signal customer_purchase
signal customer_exit

var mood_state = 0

export var walk_speed = 1.0

var counter
var state = STATE_APPROACH
var initial_position = Vector2()
var held_item = null
var original_item = null
var wait_time = 0
var total_wait_time = 0
var velocity = Vector2()
var speed = 200

var portrait = 0

var model

func init(counter, item):
	self.counter = counter
	
	self.original_item = item
	self.held_item = item

# Called when the node enters the scene tree for the first time.
func _ready():
	initial_position = position
	
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	self.model = get_node("Model" + str(rng.randi_range(1, MODELS)))
	self.model.visible = true
	self.model.connect('animation_finished', self, '_on_animation_finished')
	self.portrait = rng.randi_range(1, 5)
	
	# Randomly generate wait time
	self.total_wait_time = rand_range(MIN_WAIT_TIME, MAX_WAIT_TIME)

func _process(delta):

	if state == STATE_WAITING:
		
		if wait_time < total_wait_time:
			wait_time += delta
			
			var percentage = 1 - (wait_time / total_wait_time)
			
			$AngerMeter.set_percent(percentage)
			
			if percentage <= 0.35 and mood_state == 1:
				$AngerMeter.set_color(Color(1, 0, 0))
				emit_signal('customer_mood', self, 'angry')
				mood_state = 2
			elif percentage <= 0.75 and mood_state == 0:
				$AngerMeter.set_color(Color(1, 1, 0))
				emit_signal('customer_mood', self, 'normal')
				mood_state = 1
		else:
			var item = self.counter.peek_item()
					
			if item == self.original_item and !item.finished():
				print('time ran out. Grabbed unfinished item back.')
				$AngerMeter.visible = false
				self.held_item = self.counter.interact(null)
				state = STATE_TAKING
				self.model.play('place_up')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if state == STATE_APPROACH:
		var angle = position.angle_to_point(self.counter.position)
		velocity = Vector2.LEFT.rotated(angle)
		
		move_and_slide(velocity.normalized() * speed)
	elif state == STATE_EXIT:
		# Walk off screen
		var angle = position.angle_to_point(initial_position)
		velocity = Vector2.LEFT.rotated(angle)
		
		move_and_slide(velocity.normalized() * speed)
		
		if position.distance_to(initial_position) <= 10:
			self.state = STATE_NONE
			emit_signal('customer_exit', self)

func place_item():
	print('placing item on counter: ', counter)
	
	# Place the item on the counter
	counter.interact(self.held_item)
	self.held_item = null
	
func take_item():
	self.held_item = self.counter.interact(null)
	
	print('taking item from counter: ', self.held_item)

func _on_Area2D_body_entered(body):
	if body == self:
		return
	
	if state == STATE_APPROACH:
		# We have reached the counter
		print('counter reached')
		
		if self.counter.peek_item() == null:
			place_item()
			self.model.play('place_up')
			
		# Connect to counter item_changed
		self.counter.connect('item_changed', self, '_on_counter_item_changed')
		
		$AngerMeter.set_color(Color(0, 1, 0))
		$AngerMeter.visible = true
		
		emit_signal('customer_mood', self, 'happy')
		
		state = STATE_WAITING
		
func _on_counter_item_changed():
	print('Counter item has changed')
	
	if state == STATE_WAITING:
		var item = self.counter.peek_item()
		
		if self.held_item != null and item == null:
			# Counter opened up
			self.place_item()
			self.model.play('place_up')
			return
		
		if item != self.original_item:
			print(item)
			print(self.original_item)
			print('not my item!')
			return
			
		if !item.finished():
			print('item not finished')
			return
		
		# Grab item from counter
		self.held_item = self.counter.interact(null)
		
		assert(self.held_item != null)
		
		if wait_time >= total_wait_time:
			print('got my finished item back late!')
		else:
			print('got my finished item back on time!')
			emit_signal('customer_purchase', 10)
		
		$AngerMeter.visible = false
		
		state = STATE_TAKING
		
		self.model.play('place_up')
		
func _on_animation_finished():
	if state == STATE_WAITING:
		self.model.play('idle_up')
	elif state == STATE_TAKING:
		self.model.play('idle_down')
		state = STATE_EXIT
