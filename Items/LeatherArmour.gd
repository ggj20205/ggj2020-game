extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknLthArmr.png"),
	"fixed" : preload("res://assets/FixdLthArmr.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 0
	self.status["forge"    ].needs = 0
	self.status["grind"    ].needs = 0
	self.status["polish"   ].needs = 6
	self.status["quench"   ].needs = 0
	self.status["workbench"].needs = 4

func ready_anvil():
	return false
func ready_forge():
	return false
func ready_grind():
	return false
func ready_polish():
	return self.status["workbench"].needs <= 0
func ready_quench():
	return false
func ready_workbench():
	return true
