extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknSwrd.png"),
	"fixed" : preload("res://assets/FixdSwrd.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 4
	self.status["forge"    ].needs = 6
	self.status["grind"    ].needs = 2
	self.status["polish"   ].needs = 4
	self.status["quench"   ].needs = 2
	self.status["workbench"].needs = 0

func ready_anvil():
	return self.status["forge"].needs <= 0
func ready_forge():
	return true
func ready_grind():
	return self.status["quench"].needs <= 0
func ready_polish():
	return self.status["quench"].needs <= 0
func ready_quench():
	return self.status["anvil"].needs <= 0
func ready_workbench():
	return false
