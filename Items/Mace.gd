extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknMc.png"),
	"fixed" : preload("res://assets/FixdMc.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 6
	self.status["forge"    ].needs = 5
	self.status["grind"    ].needs = 0
	self.status["polish"   ].needs = 0
	self.status["quench"   ].needs = 3
	self.status["workbench"].needs = 0

func ready_anvil():
	return self.status["forge"].needs <= 0
func ready_forge():
	return true
func ready_grind():
	return false
func ready_polish():
	return false
func ready_quench():
	return self.status["anvil"].needs <= 0
func ready_workbench():
	return false

