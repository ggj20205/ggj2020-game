class_name Item
extends Node2D


class Status:
	var needs = 0
	var parentready
	func _init(parentready_IN, needs_IN):
		self.needs = needs_IN
		self.parentready = parentready_IN
	func do(delta := 1.0):
		if (!self.ready()):
			return true
		if (needs <= 0):
			return true
		needs -= delta
		return false
	func ready():
		return self.parentready.call_func()

func _init():
	self.texture = self.texture_preload["broke"]


func ready_anvil():
	return true
func ready_forge():
	return true
func ready_grind():
	return true
func ready_polish():
	return true
func ready_quench():
	return true
func ready_workbench():
	return true
func finished():
	return (
		self.status["anvil"    ].needs <= 0 and
		self.status["forge"    ].needs <= 0 and
		self.status["grind"    ].needs <= 0 and
		self.status["polish"   ].needs <= 0 and
		self.status["quench"   ].needs <= 0 and
		self.status["workbench"].needs <= 0
	)
func get_next_step():
	if (self.status["forge"    ].needs > 0):
		return "forge"
	if (self.status["anvil"    ].needs > 0):
		return "anvil"
	if (self.status["quench"   ].needs > 0):
		return "quench"
	if (self.status["grind"    ].needs > 0):
		return "grind"
	if (self.status["workbench"].needs > 0):
		return "workbench"
	if (self.status["polish"   ].needs > 0):
		return "polish"
	self.texture = self.texture_preload["fixed"]
	return "counter"

var status = { # Set the required number of seconds/actions at each station here
	"anvil"     : Status.new(funcref(self, "ready_anvil"    ), 0),
	"forge"     : Status.new(funcref(self, "ready_forge"    ), 0),
	"grind"     : Status.new(funcref(self, "ready_grind"    ), 0),
	"polish"    : Status.new(funcref(self, "ready_polish"   ), 0),
	"quench"    : Status.new(funcref(self, "ready_quench"   ), 0),
	"workbench" : Status.new(funcref(self, "ready_workbench"), 0),
}
var texture = null
