extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknDggr.png"),
	"fixed" : preload("res://assets/FixdDggr.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 0
	self.status["forge"    ].needs = 3
	self.status["grind"    ].needs = 1
	self.status["polish"   ].needs = 4
	self.status["quench"   ].needs = 1
	self.status["workbench"].needs = 0

func ready_anvil():
	return false
func ready_forge():
	return true
func ready_grind():
	return self.status["quench"].needs <= 0
func ready_polish():
	return self.status["quench"].needs <= 0
func ready_quench():
	return self.status["forge"].needs <= 0
func ready_workbench():
	return false
