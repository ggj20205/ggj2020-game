extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknPot.png"),
	"fixed" : preload("res://assets/FixdPot.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 2
	self.status["forge"    ].needs = 0
	self.status["grind"    ].needs = 0
	self.status["polish"   ].needs = 0
	self.status["quench"   ].needs = 0
	self.status["workbench"].needs = 0

func ready_anvil():
	return true
func ready_forge():
	return false
func ready_grind():
	return false
func ready_polish():
	return false
func ready_quench():
	return false
func ready_workbench():
	return false
