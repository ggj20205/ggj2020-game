extends Item

var texture_preload = {
	"broke" : preload("res://assets/BrknSddl.png"),
	"fixed" : preload("res://assets/FixdSddl.png"),
}

func _init():
	._init()
	self.status["anvil"    ].needs = 0
	self.status["forge"    ].needs = 0
	self.status["grind"    ].needs = 0
	self.status["polish"   ].needs = 0
	self.status["quench"   ].needs = 0
	self.status["workbench"].needs = 5

func ready_anvil():
	return false
func ready_forge():
	return false
func ready_grind():
	return false
func ready_polish():
	return false
func ready_quench():
	return false
func ready_workbench():
	return true
